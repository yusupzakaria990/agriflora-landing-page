import logo from './logo.svg';
import './App.css';
import React  from 'react';
import Navbar from './components/Navbar';
import Carousel from './components/Carousel';
import Footer from './components/Footer';

function App() {
  return (
    <React.Fragment>
        <Navbar/>
        <Carousel/>
        <Footer/>
    </React.Fragment>
  );
}

export default App;
