import React from 'react';

function Navbar() {

  return (
    <header>
        <h2><img src="/agriflora.png" alt="Logo" style={{ width: "140px", height: "auto" }} /></h2>
        
        <nav>
            <a href="/#">Home</a>
            <a href="/#">Plant</a>
            <a href="/#">Stuff</a>
            <a href="/#">Seed</a>
            <a href="/#">About</a>
        </nav>
    </header>
  );
}

export default Navbar;