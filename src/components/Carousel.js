import React, {useState} from "react"; 
import Carousel from 'react-bootstrap/Carousel';
import Card from 'react-bootstrap/Card';

import {
  MDBCarousel,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBIcon,
} from "mdb-react-ui-kit";


function ExCarousel() {
    
  return (
    <main> 
    <div className="flex-container">
      <div className="left-column"><span>discover</span>
            <h1>Your Plant</h1>
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
            <button class="cta">
                <span class="hover-underline-animation"> explore </span>
                <svg viewBox="0 0 46 16" height="10" width="30" xmlns="http://www.w3.org/2000/svg" id="arrow-horizontal">
                    <path transform="translate(30)" d="M8,0,6.545,1.455l5.506,5.506H-30V9.039H12.052L6.545,14.545,8,16l8-8Z" data-name="Path 10" id="Path_10"></path>
                </svg>
            </button>
      </div>
      <div className="right-column">

              <Carousel variant="dark">
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="3232270.jpg"
                  alt="First slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="5498828.jpg"
                  alt="Second slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="5703606.jpg"
                  alt="Third slide"
                />
              </Carousel.Item>
            </Carousel>
      </div>
    </div>
    
    <div className="features"> 
    <h4>Feature</h4>
    <p>It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum.</p>
    
      <div className="card-feature">
        <div class="grid-thirds">
        <Card style={{ width: '12rem' }}>
          <Card.Img variant="top" src="tales.png" />
          <Card.Body>
            <Card.Title>Plant</Card.Title>
          </Card.Body>
        </Card>

        <Card style={{ width: '12rem' }}>
          <Card.Img variant="top" src="pp1.png" />
          <Card.Body>
            <Card.Title>Seed</Card.Title>
          </Card.Body>
        </Card>

        <Card style={{ width: '12rem' }}>
          <Card.Img variant="top" src="garpu.png" />
          <Card.Body>
            <Card.Title>Stuf</Card.Title>
          </Card.Body>
        </Card>
        </div>
      </div>
    </div>
    
    <div className="testimonial"> 
    <h4>Testimonial</h4>
    <p>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
    <Card className="text-center">
      <Card.Body>
      <img src="/user.png" alt="Logo" style={{ width: "140px", height: "auto" }} />
        <Card.Title>Mas Kumambang</Card.Title>
        <p className="text-center font-italic">"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim"</p>
      </Card.Body>
    </Card>
    
    </div>
    
  </main>
        
  );
}

export default ExCarousel;