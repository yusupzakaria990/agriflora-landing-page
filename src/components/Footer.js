import React from 'react';

function Footer() {

  return (

    <footer class="footer-distributed">

			<div class="footer-right">

				<img src="/instagram.png" alt="Logo" style={{ width: "40px", height: "auto" }} />
				<img src="/facebook.png" alt="Logo" style={{ width: "40px", height: "auto" }} />
			</div>

			<div class="footer-left">

				<p class="footer-links">
					<a class="link-1" href="#">Home</a>

					<a href="#">Plant</a>

					<a href="#">product</a>

					<a href="#">Order</a>

					<a href="#">Faq</a>

					<a href="#">Contact</a>
				</p>

				<p>Agriflora &copy; 20203</p>
			</div>

		</footer>
  );
}

export default Footer;